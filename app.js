require('dotenv').config();
const express = require('express');
const session = require('express-session');
const { engine } = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const app = express();
const helpers = {
  switch: function(value, options) {
    this._switch_value_ = value;
    return options.fn(this);
  },
  case: function(value, options) {
    if (value === this._switch_value_) {
      return options.fn(this);
    }
  },
  default: function(options) {
    if (!this._switch_value_) {
      return options.fn(this);
    }
  }
};
// 设置Handlebars中间件
app.engine('handlebars', engine({
  defaultLayout: 'main',
  helpers: helpers
}));
app.set('view engine', 'handlebars');


// 设置body-parser中间件
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cookieParser());
// 设置静态文件夹
app.use(express.static(path.join(__dirname, 'public')));

// 配置express-session
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
}));



// 导入路由
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

// 使用路由
app.use('/', indexRouter);
app.use('/users', usersRouter);


module.exports = app;
