const fs = require('fs');
const path = require('path');
const breachedPasswordsPath = path.join(__dirname, '../config/breachedpasswords.txt');
const weakPasswordsPath = path.join(__dirname, '../config/weakpasswords.txt');


const breachedPasswords = fs.readFileSync(breachedPasswordsPath, 'utf8').split('\n');
const weakPasswords = fs.readFileSync(weakPasswordsPath, 'utf8').split('\n');


const allInvalidPasswords = Array.from(new Set([...breachedPasswords, ...weakPasswords]));
module.exports = {allInvalidPasswords}