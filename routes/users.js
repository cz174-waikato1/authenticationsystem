const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();
const {pool} = require('../database');
const {compare} = require("bcrypt");


// 注册页面
router.get('/register', (req, res) => {
    const registrationSuccess = req.cookies.registrationSuccess === 'true';
    const error = req.query.error;
    res.clearCookie('registrationSuccess');
    res.render('register', {registrationSuccess, showHeader: false, showFooter: false, error});
});


// 注册处理
router.post('/register', async (req, res) => {
    const {username, password, confirmPassword, realName, dateOfBirth} = req.body;
    const dirtyWords = require('../config/badwords.json');
    const usernameRegex = /^[a-zA-Z0-9_]+$/;


    function containsDirtyWord(string) {
        const replacedString = string.replace(/[0-9]/g, (digit) => {
            const replacements = {'0': 'o', '1': 'i', '3': 'e', '4': 'a', '5': 's', '7': 't'};
            return replacements[digit] || digit;
        });
        return dirtyWords.some(dirtyWord => replacedString.toLowerCase().includes(dirtyWord.toLowerCase()));
    }

    try {
        const result = await pool.query('SELECT * FROM UsersAuthentication WHERE LOWER(username) = LOWER(?)', [username]);
        console.log(result);
        if (result.length > 0) {
            return res.status(409).send('Username is already taken.');
        }
        if (!usernameRegex.test(username)) {
            return res.redirect('/users/register?error=invalidUsername');
        } else if (containsDirtyWord(username)) {
            return res.redirect('/users/register?error=badWords');
        }

        const {allInvalidPasswords} = require('./passwordValidation');
        if (password !== confirmPassword) {
            return res.redirect('/users/register?error=passwordMismatch');
        } else if (password.length < 8 || password.length > 64) {
            return res.redirect('/users/register?error=passwordLength');
        } else if (allInvalidPasswords.includes(password)) {
            return res.redirect('/users/register?error=weakOrBreachedPassword');
        }


        // 哈希用户密码
        const hashedPassword = await bcrypt.hash(password, 10);

        // 插入新用户到数据库
        await pool.query(
            'INSERT INTO UsersAuthentication (username, password_hash, real_name, date_of_birth) VALUES (?, ?, ?, ?)',
            [username, hashedPassword, realName, dateOfBirth]
        );
        res.cookie('registrationSuccess', 'true', {maxAge: 900000, httpOnly: true, path: '/'});
        console.log('Cookie set');
        res.redirect('/users/register');

    } catch (error) {
        console.error(error);
        res.status(500).send({success: false, message: 'Database error occurred.'});
    }
});

// 登录页面
router.get('/login', (req, res) => {
        const error = req.query.error;

        res.render('login', {error})
    }
)
;

// 登录处理
router.post('/login', async (req, res) => {
    const {username, password} = req.body;
    try {
        const result = await pool.query('SELECT * FROM UsersAuthentication WHERE LOWER(username) = LOWER(?)', [username]);
        const user = result[0];
        if (!user) {
            return res.redirect('/users/login?error=userNotFound');
        }

        const isValidPassword = await bcrypt.compare(password, user.password_hash);
        if (!isValidPassword) {

            return res.redirect('/users/login?error=incorrectPassword');
        }
     
        req.session.user = {id: user.id, username: user.username};
        req.session.loginSuccess = true; // 添加登录成功的标识
        req.session.save(() => {
            res.redirect('/home?welcome=true');
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({success: false, message: 'Internal server error.'});
    }
});

module.exports = router;
