const express = require('express');
const router = express.Router();
router.get('/', (req, res) => {
    res.render('home',{showHeader: true, showFooter: true,})
});
router.get('/users/logout', (req, res) => {
     req.session.destroy();
    res.redirect('/');
});
router.get('/home', (req, res) => {
    // 检查session中的登录成功标识或查询字符串
    const showWelcomeMessage = req.session.loginSuccess || req.query.welcome;
    console.log(showWelcomeMessage)
    if (showWelcomeMessage) {
        // 清除session中的标识以防再次显示消息
        delete req.session.loginSuccess;
    }
    res.render('home', {
        user: req.session.user,
        showWelcomeMessage,
        username: req.session.user ? req.session.user.username : '',
        showHeader: true, showFooter: true,
    });
});

module.exports = router;